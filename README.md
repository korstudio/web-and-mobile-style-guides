# Web and Mobile Style-guides

References:
- HTML/CSS
  - [Google Style Guide](https://google.github.io/styleguide/htmlcssguide.html)
  - [AirBnb Style Guide](https://github.com/airbnb/css) 
  - [cssguidelin.es](https://cssguidelin.es/)
  - [Intro to OOCSS](https://www.smashingmagazine.com/2011/12/an-introduction-to-object-oriented-css-oocss/)